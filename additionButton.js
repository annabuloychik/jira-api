var sprints = document.getElementsByClassName('js-sprint-container');

[].forEach.call(sprints, function(sprint) {

    var sprintHeader = sprint.getElementsByClassName('js-sprint-header');
    sprintId = sprintHeader[0].getAttribute('data-sprint-id');
    
    var button = document.createElement('button');
    button.className = "aui-button";
    button.innerHTML = 'Report';
    button.style = "margin-right: 8px"; 
    button.setAttribute('onClick','makeReport('+sprintId+')')

    rightGroupElement = sprintHeader[0].childNodes[3];
    rightGroupElement.insertBefore( button, rightGroupElement.firstChild);    

});