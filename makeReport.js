var sprints = document.getElementsByClassName('js-sprint-container');

[].forEach.call(sprints, function(sprint) {

    var sprintHeader = sprint.getElementsByClassName('js-sprint-header');
    sprintId = sprintHeader[0].getAttribute('data-sprint-id');
    
    var button = document.createElement('button');
    button.className = "aui-button report-button";
    button.innerHTML = 'Report';
    button.style = "margin-right: 8px"; 
    button.setAttribute('onClick','makeReport('+sprintId+')')

    rightGroupElement = sprintHeader[0].childNodes[3];
    rightGroupElement.insertBefore( button, rightGroupElement.firstChild);    

});

makeReport = function(sprintId){
    sprint = {};
    sprint.id = sprintId;

    baseUrl = 'https://jira.itransition.com';

    table = [];
    issuesWithoutEstimate = [];
    issuesWithMultipleDevelopers = [];
    issues=[];

    init = {
        credentials: 'include'
    };

    monthNamesShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    Date.prototype.getDateByFormat = function () {
        delimiter = "%2F";
        return this.getDate() + delimiter + monthNamesShort[this.getMonth()] + delimiter + ("" + this.getFullYear()).substr(2,4);
    };

    function json(response) {  
        return response.json()  
    }

    fetch(baseUrl + '/rest/agile/1.0/sprint/' + sprint.id, init)
        .then(json)
        .then(function(sprintData) {
            sprint.startDate = new Date(sprintData.startDate);
            sprint.endDate = new Date(sprintData.endDate);
            sprint.originBoardId = sprintData.originBoardId;

            return fetch(baseUrl + '/rest/api/2/search?jql=sprint%20='+sprint.id+'&expand=changelog&maxResults=1000', init);
        })
        .then(json)
        .then(function(sprintWithChangelog){
            sprint.issues = sprintWithChangelog.issues;
            return fetch(baseUrl + '/rest/agile/1.0/board/' + sprint.originBoardId + '/project', init);
        })
        .then(json)
        .then(function (board) {

            board.values.forEach(function (project) {
                fetch(baseUrl + '/rest/timesheet-gadget/1.0/raw-timesheet.json?startDate=' +
                        sprint.startDate.getDateByFormat() + '&endDate=' + sprint.endDate.getDateByFormat() + '&projectid=' + project.id, init)
                .then(json)
                .then(function (projectSprint) {

                    sprint.issues.forEach(function (issue, k) {

                        for (j = 0; j < projectSprint.worklog.length; j++) {
                            if (projectSprint.worklog[j].key == issue.key) {
                                worklog = projectSprint.worklog[j];

                                issue = {
                                    "id": issue.id,
                                    "key": issue.key,
                                    "timeOriginalEstimate": issue.fields.timeoriginalestimate,
                                    "timeSpent": issue.fields.timespent,
                                    "projectId": issue.fields.project.id,
                                    "developers": []
                                };
                                issues.push(issue);

                                addDevelopersToIssueFromWorklogEntries(issue, worklog.entries);
                                addTimeEstimateToIssueDevelopers(issue);
                                addIssueDevelopersInCommonTable(issue, table);

                                break;
                            }

                        }
                        if (k == (sprint.issues.length - 1)) {
                            console.log(table);
                        }
                    });
                });
            })
        })
        .catch(alert);

    addDevelopersToIssueFromWorklogEntries = function(issue, entries)
    {
        entries.forEach(function (entry) {

            if (issue.developers.length != 0) {
                for (i = 0; i < issue.developers.length; i++) {

                    if (issue.developers[i].name == entry.authorFullName) {
                        issue.developers[i].timeSpent += entry.timeSpent;
                        break;
                    }
                }

                if (i == issue.developers.length) {
                    issue.developers.push({
                        "name": entry.authorFullName,
                        "timeSpent": entry.timeSpent
                    });
                }
            } else {
                issue.developers.push({
                    "name": entry.authorFullName,
                    "timeSpent": entry.timeSpent
                });
            }

        });
    }

    addTimeEstimateToIssueDevelopers = function(issue) {

        if (issue.timeOriginalEstimate != null) {

            ratio = issue.timeOriginalEstimate / issue.timeSpent;

            issue.developers.forEach(function (developer) {
                developer.timeEstimate = Math.round(ratio * developer.timeSpent);
            });

        } else {
            issue.developers.forEach(function (developer) {
                developer.timeEstimate = 0;
            });
        }
    }

    addIssueDevelopersInCommonTable = function(issue, table){
        issue.developers.forEach(function (developer) {
            if (table.length != 0) {
                for (i = 0; i < table.length; i++) {
                    if (table[i].name == developer.name) {
                        table[i].timeSpent += developer.timeSpent;
                        if (issue.timeOriginalEstimate != null) {
                            table[i].timeEstimate += developer.timeEstimate;
                        } else {
                            table[i].issuesWithoutEstimate.push(issue.key);
                        }

                        break;
                    }
                }
                if (i == table.length) {
                    table.push({
                        "name": developer.name,
                        "timeSpent": developer.timeSpent,
                        "timeEstimate": developer.timeEstimate,
                        "issuesWithoutEstimate": []
                    });
                    if (issue.timeOriginalEstimate == null) {
                        table[i].issuesWithoutEstimate.push(issue.key);
                    }
                }
            } else {
                table.push({
                    "name": developer.name,
                    "timeSpent": developer.timeSpent,
                    "timeEstimate": developer.timeEstimate,
                    "issuesWithoutEstimate": []
                });
                if (issue.timeOriginalEstimate == null) {
                    table[0].issuesWithoutEstimate.push(issue.key);
                }
            }
        });
    }
}